﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTargetMover : MonoBehaviour {

	// possible states
	public enum motionDirections {Spin, Horizontal, Vertical};

	// set the state
	public motionDirections motionState = motionDirections.Horizontal;
		
	// motion parameters
	public float spinSpeed = 180.0f;
	public float movementManitude = 0.1f;
	
	// Update is called once per frame
	void Update () 
	{
		switch(motionState)
		{
		// Rotate around Y axis
		case motionDirections.Spin:
			gameObject.transform.Rotate (Vector3.up * spinSpeed * Time.deltaTime);
			break;

		// Move right and left
		case motionDirections.Horizontal:
			gameObject.transform.Translate (Vector3.right * Mathf.Cos (Time.timeSinceLevelLoad) * movementManitude);
			break;

		// Move up and down
		case motionDirections.Vertical:
			gameObject.transform.Translate (Vector3.up * Mathf.Cos (Time.timeSinceLevelLoad) * movementManitude);
			break;
		}
	}
}
